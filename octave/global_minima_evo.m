%Zum Ausfuehren Octave Konsole oeffnen und Befehl 'source('global_minima_evo.m')' eingeben. 
%Zum betrachten der Ergebnisse in der Konsole nach erfolgreicher Durchfuehrung 'Population' eingeben.
%Die Ergebnisse sind spaltenweise nach den Besten sortiert.

popSize = 50;	%Groesse der Population
generations = 500;	%Anzahl der Generationen
params = 4;
Population = (rand(params + 1,popSize) - 0.5) * 100;	%Bevoelkerung der Population
recombpercentage = 0.7;	%Schranke fuer Rekombination
mutatepercentage = 0.3; 	%Schranke fuer Mutation

bestfithistory = zeros(generations,1);

recombinecandidate = round(popSize * recombpercentage);
recombinecandidate += fmod(recombinecandidate, 2);
directcandidates = popSize - recombinecandidate;

tic;	%Zeitmassung start
for time = 1 : generations
		OldPopulation = Population;
		Population = zeros(params + 1,directcandidates);
		Population(:, 1:directcandidates) = OldPopulation(:,1:directcandidates);
		index = randperm(popSize);

		for i = 1:2:recombinecandidate
			child1 = recombine(OldPopulation(:, index(i)), OldPopulation(:, index(i+1)), "line");
			child2 = recombine(OldPopulation(:, index(i+1)), OldPopulation(:, index(i)), "line");

			if (rand() < mutatepercentage)
				child1 = mutate(child1, "all");
				child2 = mutate(child2, "randelems");
			endif

			Population = cat(2, Population, child1);
			Population = cat(2, Population, child2);
		endfor

	Population = survivalOfTheFittest(Population, popSize, "rosenbrock");

	bestfithistory(time) = Population(end,1);
endfor

duration = toc
bestFit = Population(1:end-1,1)'
fitness = Population(end,1) 
semilogy(1:generations,bestfithistory);
