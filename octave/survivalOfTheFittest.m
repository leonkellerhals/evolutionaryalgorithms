## -*- texinfo -*-
## @deftypefn {mutate.m} survivalOfTheFittest (@var{Population, popSize, mode})
## 
## @end deftypefn
function Population = survivalOfTheFittest(Population, popSize, mode)
if (nargin == 0)
	print_usage ();
endif

switch(mode)
	case "function"
		for i = 1 : columns(Population)
			gen = Population(1:end - 1, i);
			Population(end, i) = F(gen(1), gen(2), gen(3), gen(4));
		endfor
	case "gradient"
		for i = 1 : columns(Population)
			gen = Population(1:end - 1, i);
			
			g1 = 2 * (gen(1) + 10 * gen(2)) + 40 * (gen(1) - gen(4))^3;
			g2 = 20 * (gen(1) + 10 * gen(2)) + 4 * (gen(2) - 2 * gen(3))^3;
			g3 = 10 * (gen(3) - gen(4)) - 8 * (gen(2) - 2 * gen(3))^3;
			g4 = -10 * (gen(3) - gen(4)) - 40 * (gen(1) - gen(4))^3;

			Population(end, i) = (g1^2 + g2^2 + g3^2 +g4^2);
		endfor
	case "rosenbrock"
		for i = 1 : columns(Population)
			gen = Population(1:end - 1, i);
			Population(end, i) = rosenbrock(gen);
		endfor
	otherwise
		error("Invalid input for mode");
endswitch

[~, FittestIndex] = sort(Population(end,:), 'ascend');
FittestIndex = FittestIndex(1:popSize);

nextGen = ones	(rows(Population),popSize);
for i = 1 : popSize
	nextGen(:, i) = Population(:,FittestIndex(i));
endfor
Population = nextGen;

endfunction
