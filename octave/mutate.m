## -*- texinfo -*-
## @deftypefn {mutate.m} mutate (@var{gen, mode})
## 
## @end deftypefn
function gen = mutate(gen, mode)
if (nargin == 0)
	print_usage ();
endif

switch (mode)
	case "all" %change all allele
		for i = 1:length(gen)
			faktor = (rand()-0.5) * 2 * gen(i);
			if (abs(faktor) < 1)
				s = sign(faktor);
				faktor /= faktor;
				faktor *= s;
			endif
			gen(i) += faktor;
		endfor
	case "randelems" %change random allele, at least one
		number = round(rand() * (length(gen) - 2) + 1);
		mIdx = randperm(length(gen(1:end-1)));
		for i = mIdx(1:number)
			gen(i) += sign(rand()-0.5) * 2;
		endfor
	otherwise
		error ("Invalid input for mode")
endswitch

endfunction
