function y = F(x_1, x_2, x_3,x_4)
y = 0;

y += (x_1 + 10 * x_2)^2;
y += 5 * (x_3 -x_4)^2;
y += (x_2 - 2 * x_3)^4;
y += 10 * (x_1 -x_4)^4;

endfunction 
