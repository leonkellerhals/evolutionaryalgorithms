function y = rosenbrock(genom)

y = 0;

for i = 1:length(genom)-1
	y += 100 * (genom(i)^2 - genom(i+1))^2 + (1-genom(i))^2;
endfor

endfunction