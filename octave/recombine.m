## -*- texinfo -*-
## @deftypefn {recombine.m} recombine (@var{parent1, parent2, mode})
## 
## @end deftypefn
function child = recombine(parent1, parent2, mode)
if (nargin == 0)
	print_usage ();
endif

child = zeros(size(parent1));

switch(mode)
	case "mean" %arithmetic mean of parent allele
		child =  (parent1 + parent2)/2;
	case "line" %Random point on the line spanned by parent allele
		koeff = rand() * 0.5 + 0.25;
	 	for i = 1:length(parent1)
		 	child(i) = koeff * parent1(i) + (1 - koeff) * parent2(i);
		endfor
	otherwise
		error ("Inalid input for mode");
endswitch	

endfunction
