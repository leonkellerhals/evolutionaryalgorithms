from sympy import Symbol
from random import random
from helpers import *

class Individual(object):

    x1_sym = Symbol('x1')
    x2_sym = Symbol('x2')
    x3_sym = Symbol('x3')
    x4_sym = Symbol('x4')
    f = (x1_sym + 10*x2_sym)**2 + 5*(x3_sym - x4_sym)**2 + \
        (x2_sym - 2*x3_sym)**4 + 10*(x1_sym - x4_sym)**4

    @classmethod
    def calculate_y(class_obj, x1, x2, x3, x4):
        return class_obj.f.evalf(subs={
            class_obj.x1_sym: x1,
            class_obj.x2_sym: x2,
            class_obj.x3_sym: x3,
            class_obj.x4_sym: x4
            }
        )

    def __init__(self, x1, x2, x3, x4):
        self.x1 = x1
        self.x2 = x2
        self.x3 = x3
        self.x4 = x4
        self.y = Individual.calculate_y(x1, x2, x3, x4)

    def reproduce(self, other):
        return Individual(
            mean(self.x1, other.x1),
            mean(self.x2, other.x2),
            mean(self.x3, other.x3),
            mean(self.x4, other.x4)
        )

    def mutate(self):
        self.x1 = -1 + random() * 2
        self.x2 = -1 + random() * 2
        self.x3 = -1 + random() * 2
        self.x4 = -1 + random() * 2
        self.y = Individual.calculate_y(self.x1, self.x2, self.x3, self.x4)

    def __lt__(self, other):
      return self.y < other.y
