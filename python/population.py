from individual import Individual
from random import random

class Population(object):

    def __init__(self, interval, number):
        self.lower_bound = interval[0]
        self.upper_bound = interval[1]
        self.delta = self.upper_bound - self.lower_bound
        self.number = number
        self.individuals = []
        for _ in range(number):
            self.individuals.append(Individual(
                self.lower_bound + random() * self.delta,
                self.lower_bound + random() * self.delta,
                self.lower_bound + random() * self.delta,
                self.lower_bound + random() * self.delta
                )
            )

    def reproduction(self):
        percentage = 0.7
        self.individuals.sort()
        fittest = self.individuals[:int(self.number*percentage)]
        children = []
        for i in range(len(fittest)-1):
            children.append(fittest[i].reproduce(fittest[i+1]))
        self.individuals.extend(children)

    def natural_selection(self):
        self.individuals.sort()
        del self.individuals[self.number:]

    def mutation(self):
        percentage = 0.3
        worst = self.individuals[-int(self.number*percentage):]
        for individual in worst:
            individual.mutate()

    def show(self):
        print("\nPrinting %d individuals...\n" % len(self.individuals))
        for i in self.individuals:
            print("x1: % f\tx2: % f\tx3: % f\tx4: % f\ny : % s\n" % (
                    i.x1, i.x2, i.x3, i.x4, i.y
                )
            )

