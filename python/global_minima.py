#!/usr/bin/env python3.4

from population import Population

interval = -5, 5
pop_count = 20
population = Population(interval, pop_count)
population.show()
population.reproduction()
population.show()
population.natural_selection()
population.show()
population.mutation()
population.show()
